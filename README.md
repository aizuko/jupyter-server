# JupyterHub for ARVP

This is the JupyterHub configuration we use for [ARVP](https://arvp.org).

Code largely forked from [jupyterhub-deploy-docker](https://github.com/jupyterhub/jupyterhub-deploy-docker).

## Launching

First modify `jupyterhub_config.py` to your liking, especially the bind-mount
path for `c.DockerSpawner.volumes`. Modify `docker-compose.yml` as required.

```bash
docker compose up -d
```

Then go to [https://localhost:9025](https://localhost:9025), where the
JupyterHub server will be accessible. By default, there should only be an
`akemi` user.

## Issues

The largest issue comes from generating the bind-mount for a new user.
Currently, while the directory will be created for the user, it'll be owned by
root. That prevents said user from writing anything to that directory, so it
requires a server administrator to run `chown` on that user's directory once.

The issue can be avoided by using a docker volume, but that's not persistent,
which is far more important to us. You can modify the `c.DockerSpawner.volumes`
line in `jupyterhub_config.py` to change this.
